import java.util.Scanner;

public class InputFromKeyboard {

    public static void main(String[] args){
        //System.out --> in ra console
        //System.in --> nhan du lieu tu ban phim
        
        //1.khoi tao doi tuong scanner
        Scanner sc = new Scanner(System.in);


        System.out.println("Nhap ten cua ban:");
        String name = sc.nextLine();
        

        System.out.println("Nhap tuoi cua ban:");
        int age = sc.nextInt();

        System.out.println("Nhap chieu cao cua ban:");
        double height = sc.nextDouble();

        System.out.println("Xin chao "+name +",ban " + age +" tuoi va cao " + height +" met.");
        sc.close();
    }
    
}
