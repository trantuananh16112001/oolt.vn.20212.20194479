import java.util.Scanner;


public class Triangle {

    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);

        System.out.println("Nhap vao chieu cao cua tam giac:");
        int height = sc.nextInt();

        System.out.println("chuong trinh se in ra tam giac can co chieu cao la " + height);

        if(height >0){
            for (int i=1; i<=height;i++){
                //in dau space
                for(int j=1; j <= height - i ;j++){
                    System.out.print(" ");
                }
                //in dau *
                for (int k =1; k<=2*i-1;k++ ){
                    System.out.print("*");
                }
                //in dau xuong dong
                System.out.println();
            }
        }
        sc.close();
    }   
}
