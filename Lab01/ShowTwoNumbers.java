// ex 4: show two number from user input numbers
import javax.swing.JOptionPane;

public class ShowTwoNumbers {
    public static void main(String[] args) {
        String num1, num2;

        num1 = JOptionPane.showInputDialog(null,"input the first number:","Enter the first number",
                                            JOptionPane.INFORMATION_MESSAGE);
        num2 = JOptionPane.showInputDialog(null,"input the second number:","Enter the second number",
                                            JOptionPane.INFORMATION_MESSAGE);

        String result = "You entered: " + num1 + " and " + num2;

        JOptionPane.showMessageDialog(null, result, "show two numbers", JOptionPane.INFORMATION_MESSAGE);

        System.exit(0);
    }
}