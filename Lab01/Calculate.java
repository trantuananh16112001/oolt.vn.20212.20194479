//ex 5:  calculate sum, difference, product, and quotient of 2 double numbers which are entered by users

import javax.swing.JOptionPane;

public class Calculate {
    public static void main(String[] args) {
        String result ="u've just entered ";
        String strnum1,strnum2;
        strnum1=JOptionPane.showInputDialog(null,"pls input the 1st number:","input the 1st number",
                                            JOptionPane.INFORMATION_MESSAGE);
        result += strnum1 + " and ";
        strnum2=JOptionPane.showInputDialog(null,"pls input the 2nd number:","input the 2nd number",
                                            JOptionPane.INFORMATION_MESSAGE);
        result += strnum2;
        double num1 = Double.parseDouble(strnum1);
        double num2 = Double.parseDouble(strnum2);

        result += "\nSum: " + (num1 + num2) 
                    + "\nDifference: " + (num1 - num2) 
                    + "\nProduct: " + (num1*num2) 
                    + "\nQuotient: ";

        if(num2 == 0){
           result += "Error! The second number is 0!";
        }
        else {
           result += (num1/num2);
        }

        JOptionPane.showMessageDialog(null,result, "Show result", JOptionPane.INFORMATION_MESSAGE);

        System.exit(0);
    }
}